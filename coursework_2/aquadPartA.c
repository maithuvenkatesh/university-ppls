/* Implementation details

Farmer:
The farmer first loads the initial set of points and sends this to the first worker as a task.

The farmer operates a main loop which continues as long as there are still tasks left
to complete or there there workers still processing tasks. The farmer checks for messages from
the workers before assigning any further work. 
MPI_Probe is a blocking test for a message and allows the farmer to check for messages without actually
receiving it. Given that an initial task was already sent out, a result is expected by the farmer. 
MPI_Probe was used for this reason instead of MPI_Iprobe which is non-blocking. In the first iteration of 
the loop, the farmer will always expect a message from a worker hence needs to look for it before continuing.
The farmer will either receive a RESULT_AREA_TAG or RESULT_TASKS_TAG from the workers. The
RESULT_AREA_TAG will cause the farmer to get the calculated area from the message and add it to the
area it is keeping track of. Receiving the RESULT_TASK_TAG makes the farmer build two new subtasks using the
three MPI_DOUBLES received and pushes them onto the tasks stack. 

The farmer then iterates through the workers and checks which are free so that it can send them tasks.
MPI_Send is used to send the points to the workers. 

Once out of the main loop the farmer iterates through the workers and send a message using MPI_SEND
instructing them to end using the TERMINATE_TAG.

Worker:
The worker processes also operate a similar main loop to the farmer. This loop iterates until the
worker receives a message from the farmer instructing it to terminate. The worker uses MPI_Probe to 
check for messages from the farmer. The reason for using a blocking call was because the worker
has no task to complete until the farmer has sent it one. Once the farmer sends the worker a message,
MPI_Recv is used to receive the message using the appropriate tag (either TERMINATE_TAG or EXECUTE_TASK_TAG). 
Receiving the EXECUTE_TASK_TAG causes the worker process to get the data points from the message and
compute the quadrature approximation. If this is an accepted area then the worker uses MPI_Send to pass the 
area back to the farmer. However if the area is not accepted then the worker sends back three MPI_DOUBLES to
the farmer. An obvious optimisation would be for the farmer to cache the coordinates and use those values to
compute the subtask itself rather than sending back three MPI_Doubles in the message. This implementation
does not support that. If the TERMINATE_TAG was received by the worker then it breaks out the loop.

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include "stack.h"

#define EPSILON 1e-3
#define F(arg)  cosh(arg)*cosh(arg)*cosh(arg)*cosh(arg)
#define A 0.0
#define B 5.0

#define SLEEPTIME 1

/* MPI tags */
#define TERMINATE_WORKER_TAG 1
#define EXECUTE_TASK_TAG 2
#define RESULT_AREA_TAG 3
#define RESULT_TASKS_TAG 4

int *tasks_per_process;

double farmer(int);

void worker(int);

int main(int argc, char **argv ) {
  int i, myid, numprocs;
  double area, a, b;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  if(numprocs < 2) {
    fprintf(stderr, "ERROR: Must have at least 2 processes to run\n");
    MPI_Finalize();
    exit(1);
  }

  if (myid == 0) { // Farmer
    // init counters
    tasks_per_process = (int *) malloc(sizeof(int)*(numprocs));
    for (i=0; i<numprocs; i++) {
      tasks_per_process[i]=0;
    }
  }

  if (myid == 0) { // Farmer
    area = farmer(numprocs);
  } else { //Workers
    worker(myid);
  }

  if(myid == 0) {
    fprintf(stdout, "Area=%lf\n", area);
    fprintf(stdout, "\nTasks Per Process\n");
    for (i=0; i<numprocs; i++) {
      fprintf(stdout, "%d\t", i);
    }
    fprintf(stdout, "\n");
    for (i=0; i<numprocs; i++) {
      fprintf(stdout, "%d\t", tasks_per_process[i]);
    }
    fprintf(stdout, "\n");
    free(tasks_per_process);
  }
  MPI_Finalize();
  return 0;
}

double farmer(int numprocs) {
  MPI_Status status;
  double area;
  double initial_points[2];
  stack *tasks_stack;
  int i, no_of_workers, received_tag, process_working_count;

  no_of_workers = numprocs-1;
  process_working_count = 0;
  int workers[numprocs];

  /* Set all workers as free */
  for(i = 1; i < no_of_workers; i++) {
    workers[i] = 0;
  }

  /* Put initial points into array and send it to worker */
  tasks_stack = new_stack();
  initial_points[0] = A;
  initial_points[1] = B;

  MPI_Send(initial_points, 2, MPI_DOUBLE, 1, EXECUTE_TASK_TAG, MPI_COMM_WORLD);
  workers[1] = 1;
  process_working_count++;

  area = 0.0;
  while(!is_empty(tasks_stack) || process_working_count > 0) {
    
    /* Check for messages from workers before sending out new tasks */
    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    received_tag = status.MPI_TAG;

    if(received_tag == RESULT_AREA_TAG) {
      /* Get the calculate area from message and add it to the area */
      double received_area;
      MPI_Recv(&received_area, 1, MPI_DOUBLE, MPI_ANY_SOURCE, RESULT_AREA_TAG, MPI_COMM_WORLD, &status);

      area += received_area;
      process_working_count--;
      workers[status.MPI_SOURCE] = 0;
    }
    else if(received_tag == RESULT_TASKS_TAG) {
      /* Get the coordinates from message and add two new tasks to the stack */
      double left, right, mid;
      double recv_data[3];
      double left_task[2];
      double right_task[2];
      MPI_Recv(&recv_data, 3, MPI_DOUBLE, MPI_ANY_SOURCE, RESULT_TASKS_TAG, MPI_COMM_WORLD, &status);

      left = recv_data[0];
      mid = recv_data[1];
      right = recv_data[2];

      left_task[0] = left;
      left_task[1] = mid;
      push(left_task, tasks_stack);

      right_task[0] = mid;
      right_task[1] = right;
      push(right_task, tasks_stack);

      process_working_count--;
      workers[status.MPI_SOURCE] = 0;
    }

    /* Send tasks to works */
    double *task_data;
    for(i = 1; i < no_of_workers + 1; i++){
      if(is_empty(tasks_stack)){
        break;
      }
      else if(workers[i] == 0) {
        task_data = pop(tasks_stack);
        MPI_Send(task_data, 2, MPI_DOUBLE, i, EXECUTE_TASK_TAG, MPI_COMM_WORLD);
        
        tasks_per_process[i]++;
        process_working_count++;
        workers[i] = 1;
      }
    }
  }

  /* Terminate workers */
  for(i = 1; i < no_of_workers+1; i++) {
    MPI_Send(NULL, 0, MPI_INT, i, TERMINATE_WORKER_TAG, MPI_COMM_WORLD);
  }
  return area;
}

void worker(int mypid) {
  MPI_Status status;
  int received_tag;

  while(1) {
    /* Wait for command from farmer */  
    MPI_Probe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    received_tag = status.MPI_TAG;

    /* No more tasks to be completed */
    if(received_tag == TERMINATE_WORKER_TAG) {
      MPI_Recv(NULL, 0, MPI_INT, 0, TERMINATE_WORKER_TAG, MPI_COMM_WORLD, &status);
      break;
    }
    else if(received_tag == EXECUTE_TASK_TAG) {
      double task_data[2];
      double left, right, left_area, right_area, fleft, fright, fmid, lrarea, mid;
      
      MPI_Recv(&task_data, 2, MPI_DOUBLE, 0, EXECUTE_TASK_TAG, MPI_COMM_WORLD, &status);
      left = task_data[0];
      right = task_data[1];
      mid = (left + right)/2;

      /* Sleep as required */
      usleep(SLEEPTIME);

      /* Quadrature algorithm */
      fleft = F(left);
      fright = F(right);
      fmid = F(mid);
      left_area = (fleft + fmid) * (mid - left) / 2;
      right_area = (fmid + fright) * (right - mid) / 2;
      lrarea = (fleft + fright) * (right - left) / 2;

      if(fabs((left_area + right_area) - lrarea) > EPSILON) {
        /* Send message to tell farmer with data for new tasks */
        double new_tasks_data[3];
        new_tasks_data[0] = left;
        new_tasks_data[1] = mid;
        new_tasks_data[2] = right;

        MPI_Send(&new_tasks_data, 3, MPI_DOUBLE, 0, RESULT_TASKS_TAG, MPI_COMM_WORLD);
      }
      else {
        double total_area;
        total_area = left_area + right_area;
        MPI_Send(&total_area, 1, MPI_DOUBLE, 0, RESULT_AREA_TAG, MPI_COMM_WORLD);
      }
    }
  }
}