/* Implementation details

Farmer:
The farmer divides the task w chunks where w is the number of workers. MPI_Send is used to send the data points to each of the worker
processes using the EXECUTE_TASK_TAG. The farmer then iterates through the number of workers and uses MPI_Recv to receive the computed
area and the number of tasks taken by each worker to compute the area. It then adds the area received to the total area and increments
the tasks_per_process for the worker using the count it received from the worker. MPI_Recv was used because it is a blocking call and
the farmer needs to receive two messages from each worker.

Worker:
Each worker uses MPI_Recv to get the task data from the farmer. MPI_Recv is a blocking call and is used here because each worker has to
wait for data before it can do anything. Hence it waits for a message with the EXECUTE_TASK_TAG from the farmer. Once it received the 
data from the farmer, the worker process calls the quad function and includes a pointer to a counter to keep a track of the 
number of tasks completed by each worker. The worker then sends back two messages with two separate tags to the farmer. 
The RESULT_PROCESSES_TAG is used to send the farmer the count of the number of tasks taken by the worker to compute the area. 
The RESULT_AREA_TAG is used to the farmer the area computed.

Evaluation:
Sending and receiving two messages per worker process is expensive wrt to communication costs. This could be implemented using 
MPI_Scatter and MPI_Gather. MPI_Scatter contains a buffer with all the tasks to be distributed to the workers and MPI_Gather gathers
together all the results from the worker processes. However this is not implemented here as I had problems when trying to implement
this.

*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include "stack.h"

#define EPSILON 1e-3
#define F(arg)  cosh(arg)*cosh(arg)*cosh(arg)*cosh(arg)
#define A 0.0
#define B 5.0

#define SLEEPTIME 1

#define EXECUTE_TASK_TAG 1
#define RESULT_AREA_TAG 2
#define RESULT_PROCESSES_TAG 3

int *tasks_per_process;

double farmer(int);

void worker(int);

double quad(double, double, double, double, double, int *);

int main(int argc, char **argv ) {
  int i, myid, numprocs;
  double area, a, b;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  if(numprocs < 2) {
    fprintf(stderr, "ERROR: Must have at least 2 processes to run\n");
    MPI_Finalize();
    exit(1);
  }

  if (myid == 0) { // Farmer
    // init counters
    tasks_per_process = (int *) malloc(sizeof(int)*(numprocs));
    for (i=0; i<numprocs; i++) {
      tasks_per_process[i]=0;
    }
  }

  if (myid == 0) { // Farmer
    area = farmer(numprocs);
  } else { //Workers
    worker(myid);
  }

  if(myid == 0) {
    fprintf(stdout, "Area=%lf\n", area);
    fprintf(stdout, "\nTasks Per Process\n");
    for (i=0; i<numprocs; i++) {
      fprintf(stdout, "%d\t", i);
    }
    fprintf(stdout, "\n");
    for (i=0; i<numprocs; i++) {
      fprintf(stdout, "%d\t", tasks_per_process[i]);
    }
    fprintf(stdout, "\n");
    free(tasks_per_process);
  }
  MPI_Finalize();
  return 0;
}

double farmer(int numprocs) {
  MPI_Status status;
  double area;
  int i, no_of_workers, received_tag, process_working_count;
  double left, right, size;

  no_of_workers = numprocs - 1;

  /* Get initial points */
  left = A;
  right = B;

  /* Get the size of each chunk */
  size = (right-left)/no_of_workers;

  for(i = 0; i < no_of_workers; i++) {
    double task_data[2];
    task_data[0] = size * i;
    task_data[1] = size * i + size;

    /* Send task to worker */
    MPI_Send(&task_data, 2, MPI_DOUBLE, i+1, EXECUTE_TASK_TAG, MPI_COMM_WORLD);
  }

  /* Check for messages while there are worker processes computing the area */
  area = 0.0;
  for(i = 0; i < no_of_workers; i++) {
    double received_area;
    int tasks_count;

    MPI_Recv(&received_area, 1, MPI_DOUBLE, MPI_ANY_SOURCE, RESULT_AREA_TAG, MPI_COMM_WORLD, &status);
    area += received_area;

    MPI_Recv(&tasks_count, 1, MPI_INT, MPI_ANY_SOURCE, RESULT_PROCESSES_TAG, MPI_COMM_WORLD, &status);
    tasks_per_process[status.MPI_SOURCE] = tasks_count;
  }
  return area; 
}

void worker(int mypid) {
  MPI_Status status;
  int received_tag;
  double area;
  int task_counter = 0;
  double task_data[2];
  double left, right, fleft, fright, lrarea;
  
  MPI_Recv(&task_data, 2, MPI_DOUBLE, 0, EXECUTE_TASK_TAG, MPI_COMM_WORLD, &status);
  left = task_data[0];
  right = task_data[1];
  fleft = F(left);
  fright = F(right);
  lrarea = (fleft + fright) * (right - left) / 2;

  /* Sleep as required */
  usleep(SLEEPTIME);

  area = quad(left, right, fleft, fright, lrarea, &task_counter);
  MPI_Send(&area, 1, MPI_DOUBLE, 0, RESULT_AREA_TAG, MPI_COMM_WORLD);
  MPI_Send(&task_counter, 1, MPI_INT, 0, RESULT_PROCESSES_TAG, MPI_COMM_WORLD);
}

double quad(double left, double right, double fleft, double fright, double lrarea, int* task_counter) {
  double mid, fmid, larea, rarea, area;
  mid = (left + right) / 2;
  fmid = F(mid);
  fleft = F(left);
  (*task_counter)++;

  larea = (fleft + fmid) * (mid - left) / 2;
  rarea = (fmid + fright) * (right - mid) / 2;
  if( fabs((larea + rarea) - lrarea) > EPSILON ) {
    larea = quad(left, mid, fleft, fmid, larea, task_counter);
    rarea = quad(mid, right, fmid, fright, rarea, task_counter);
  }
  return (larea + rarea);
}